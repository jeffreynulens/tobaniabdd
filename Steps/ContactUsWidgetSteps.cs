﻿using TobaniaTesting.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TechTalk.SpecFlow;

namespace TobaniaTesting
{
    [Binding]
    public class ContactUsWidgetSteps
    {
        [Given(@"Gert is on the homepage")]
        public void GivenGertIsOnTheHomepage()
        {
            //nothing to do because homepage is already loaded in before scenario hook
        }

        [When(@"Gert clicks Contact Us")]
        public void WhenTheUserClicksContactUs()
        {
            Page.HomePage.ClickContactUsButton();
        }
        
        [Then(@"the Contact Us Widget is opened")]
        public void ThenTheContactUsWidgetIsOpened()
        {
            Assert.IsTrue(Page.ContactUsWidget.IsContactWidgetDisplayed());
        }
    }
}
