﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobaniaTesting.PageObjects
{
    class HomePage
    {
        [FindsBy(How = How.Id, Using = "contact-link")]
        private IWebElement contactUsButton { get; set; }

        public HomePage ClickContactUsButton()
        {
            contactUsButton.Click();
            return this;
        }
    }
}
